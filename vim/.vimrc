" Vim

" Compatibility options {{{1
" Don't act like old guy vi, give me the features of vim
if &compatible
  set nocompatible
endif
" When moving the cursor up or down just after inserting indent for 'autoindent',
" do not delete the indent.
set cpoptions+=I
" Allow editing command pasted from clipboard using :@r (where r is register)
set cpoptions-=e
" When using "w!" while the 'readonly' option is set, don't reset 'readonly'.
" set cpoptions+=Z

" Debugging Vim {{{1
" Toggle verbosity - useful when debugging issues
function! ToggleVerbose()
  if !&verbose
    set verbosefile=~/vimlog/verbose.log
    set verbose=15
  else
    set verbose=0
    set verbosefile=
  endif
endfunction

" Fundamental Pre-Plugin options {{{1
" UTF-8 FTW!
set encoding      =utf-8
set fileencoding  =utf-8
set fileencodings =ucs-bom,utf8

" Enable omnicomplete FIXME
set omnifunc=syntaxcomplete#Complete

" Space is the <leader> key
let mapleader=" "
" And Tab the <localleader> key (used for Todo.txt)
let maplocalleader="	"

" Clear old autocmd's (when this is loading by :so for eg.)
" Do them all together at the top so you don't need to worry about reordering
" augroups that are split in multiple places
autocmd!
augroup general_vim_au
  autocmd!
augroup END
augroup filetype_au
  autocmd!
augroup END
" Vundle package management {{{1
" Add .vim to rtp even on Windows, to get non-Vundle changes picked up
set rtp+=~/.vim/

let vundle_readme=expand('~/.vim/bundle/Vundle.vim/README.md')
if filereadable(vundle_readme)
  let iCanHazVundle=1

  filetype off                   " required!
  set rtp+=~/.vim/bundle/Vundle.vim/
  call vundle#begin()

  " let Vundle manage Vundle
  " required!
  Plugin 'VundleVim/Vundle.vim'

  "  General editing plugins {{{3
  Plugin 'tpope/vim-surround.git'      " Maybe replace with machakann/vim-sandwich
  Plugin 'tpope/vim-repeat.git'
  Plugin 'tpope/vim-unimpaired.git'    " for ]<Space>, [e, [oh, etc.
  Plugin 'tpope/vim-characterize.git'
  Plugin 'tpope/vim-abolish'
  Plugin 'tpope/vim-speeddating'
  Plugin 'tpope/vim-jdaddy'            " JSON prettyprinting, manip as objects
  Plugin 'tpope/vim-scriptease'        " for :Scriptnames, other debugging
  Plugin 'tyru/capture.vim'            " :capture this-command's-output in buffer
  Plugin 'wellle/targets.vim'          " mainly for the separator maps
  Plugin 'godlygeek/tabular'
  Plugin 'ctrlpvim/ctrlp.vim'
  Plugin 'sjl/gundo.vim'
  Plugin 'vim-scripts/compview'
  Plugin 'othree/eregex.vim'
  Plugin 'bling/vim-airline'
  Plugin 'mjbrownie/swapit'           " to ctrl-a true to false
  "Plugin 'dkprice/vim-easygrep'       " TODO: RTFM, configure, enable
  "Plugin 'mileszs/ack.vim'
  "Plugin 'mg979/vim-visual-multi'      " TODO: RTFM
  Plugin 'dkarter/bullets.vim'

  " Support for specific tools {{{3
  Plugin 'lervag/wiki.vim'
  Plugin 'tylerszabo/todo.txt-vim'     " github mirror of dbeniamine/todo.txt-vim on Gitlab
  Plugin 'ledger/vim-ledger'
  "Plugin 'wannesm/wmgraphviz.vim'
  Plugin 'tpope/vim-fugitive'

  " Colorschemes {{{3
  Plugin 'zanloy/vim-colors-sunburst'   "dark
  Plugin 'ajgrf/sprinkles'              "both light and dark
  Plugin 'acarapetis/vim-colors-github' "light
  Plugin 'ParamagicDev/vim-medic_chalk'
  Plugin 'jaredgorski/fogbell.vim'

  " Programming specific plugins {{{3
  Plugin 'chrisbra/matchit'
  "  dense-analysis/ale for async syntax linting 
  "  Shougo/deoplete.nvim for async completions

  " Snippets
  Plugin 'SirVer/ultisnips'
  Plugin 'digital-carver/vim-snippets'    "fork of honza/... edited to my preference
  set rtp+=~/.vim/my-snippets/

  " FileType support
  Plugin 'othree/html5.vim'
  Plugin 'elzr/vim-json'
  Plugin 'rust-lang/rust.vim'
  Plugin 'avakhov/vim-yaml'
  Plugin 'vim-ruby/vim-ruby'
  " Optionally run `make clean carp dancer highlight-all-pragmas moose test-more try-tiny`
  "  in vim-perl folder
  Plugin 'vim-perl/vim-perl'
  Plugin 'plasticboy/vim-markdown'
  Plugin 'JuliaEditorSupport/julia-vim'
  Plugin 'zaid/vim-rec'

  call vundle#end()
endif
" Indent automatically according to filetype
filetype plugin indent on
" Internal plugin load
packadd! editexisting

" General (non-plugin-specific, non-filetype-specific) Vim settings {{{1
" Syntax highlighting
syntax on

" Load external file changes automatically
set autoread

" Line at 80th column just for a reference
"set colorcolumn=80
augroup general_vim_au
  " Do it after the ColorScheme is set, so we can override its setting
  autocmd ColorScheme * if &background == "dark" |
        \ highlight ColorColumn guibg=#101010 ctermbg=DarkGray |
        \ else |
        \ highlight ColorColumn guibg=#FCEFCF ctermbg=LightGray

" Set colors of folded lines to be more pleasant
  autocmd ColorScheme * highlight Folded ctermfg=DarkMagenta
  autocmd ColorScheme * highlight Folded guifg=DarkMagenta
augroup END

" Set colorscheme according to time of day
function! AutoDarkModeChange(timer)
  let hourofday = strftime("%H") 
  if hourofday > 7 && hourofday < 18
    set background=light
    try
      colorscheme sprinkles
    catch /E185/ " colorscheme does not exist
      colorscheme delek
    endtry
  else
    set background=dark
    try
      colorscheme sunburst
    catch /E185/ " colorscheme does not exist
      colorscheme murphy
      "ron is good too
    endtry
  endif
endfunction

function! AutoDarkModeSetup()
  " Check once every 15 min
  let timer = timer_start(900000, 'AutoDarkModeChange', {'repeat': -1})
  call AutoDarkModeChange(timer) " Initial call to setup the theme
endfunction

call AutoDarkModeSetup()

" Backups are not in your job description, thankyouverymuch
set nobackup

" Highlight some special lines
let matchIDSpecialComment = matchadd('SpecialComment', '^\s*#\s*HACK.*')

" search for tags in current dir, then upwards
set tags=./tags;/,tags;/

set autoindent
" Always use just \n as newline, not \r\n
set fileformat=unix
autocmd BufEnter set fileformat=unix
set fileformats=unix,dos

" Do not store global and local option values in a session, since refreshing
" them via .vimrc is often the whole point
set sessionoptions-=options

" Line numbers
set number
set relativenumber

" Keep a few extra lines visible when scrolling
set scrolloff=3

" Always show status line
set laststatus=2

" Filename as you typed, whether modified, readonly, filetype, line,column,
" percentage, total no. of lines
set statusline=%n\ %f\ %m%y\ %l,%c\ %p%%(Total:%L)\ \ \ %r

" Change tabs to spaces
set expandtab
" Change a tab to 2 spaces
set tabstop=2
" Indent to 2 spaces (in autoindent, etc.)
set shiftwidth=2

" Don't auto-commentify the next line after a comment
set formatoptions-=ro
if v:version >= 740
  "When joining lines, try to join comments intelligently (only in v7.4+)
  set formatoptions+=j
endif

" Do incremental searching when it's possible to timeout.
if has('reltime')
  set incsearch
endif

" Don't recognize octal numbers for Ctrl-A and Ctrl-X, ends up being confusing.
set nrformats-=octal

" Enable mouse actions in terminal
if has('mouse')
  set mouse=a
endif

" Substitutions happen over whole line by default, add /g to do only one sub
set gdefault

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

" Store many commands - default is 50
set history=500

" Show matching brackets
set showmatch

" Highlight search results (use <leader>l to clear these, ref: #tn=nohlsearch )
set hlsearch

" paste mode - this will avoid unexpected effects (unnecessary indentation) when
" you cut or copy some text from one window and paste it in Vim.
set pastetoggle=<F11>

" Avoid creating temporary files in source directories
if ! isdirectory($HOME . "/vimtmp")
  call mkdir($HOME . "/vimtmp")
endif

" Swap file and Undo file directories. Maybe should use set ...^= instead?
if isdirectory($HOME . "/vimtmp")
  set directory=$HOME/vimtmp//,$HOME/tmp//,$TEMP//
  set undodir=$HOME/vimtmp/,$HOME/tmp/,$TEMP/
else
  set directory=$HOME/tmp//,$TEMP//,.
  set undodir=$HOME/tmp/,$TEMP/
endif

augroup general_vim_au
  " Enable undo-ing of :bdelete
  autocmd BufDelete * let g:latest_deleted_buffer = expand("<afile>:p:gs?\\?/?")
  nnoremap <C-S-t> :e <C-R>=g:latest_deleted_buffer<CR><CR>

  " Remove stupid comment-related formatoptions set by ftplugins
  autocmd BufNewFile,BufRead * setlocal formatoptions-=ro

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid, when inside an event handler
  " (happens when dropping a file on gvim) and for a commit message (it's
  " likely a different one than last time).
  autocmd BufReadPost *
        \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ |   exe "normal! g`\""
        \ | endif


  " Only use cursorline for current window, except when in diff mode
  " from https://github.com/lervag/dotvim/blob/master/vimrc
  autocmd WinEnter,FocusGained * if !&diff | setlocal cursorline | endif
  autocmd WinLeave,FocusLost   * if !&diff | setlocal nocursorline | endif
augroup END

" When tab completion has multiple completions, complete upto
" longest common prefix and show options
set wildmode=full
set wildmenu

" Internal keycodes sent by terminals should arrive fairly quickly anyway
set ttimeout		    " time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key

" Open help in new tab
cnoreabbrev <expr> h getcmdtype() == ":" && getcmdline() == 'h' ?
      \ 'tab help' :
      \ 'h'
function! GetHelpOnCwordInTab()
  if &filetype == "vim"
    execute 'tab help ' . expand("<cword>")
  else
    execute 'tabnew | read ! ' . &keywordprg . expand("<cword>")
  endif
endfunction
autocmd general_vim_au FileType * nnoremap <C-K> :call GetHelpOnCwordInTab()<CR>

" Ignore case generally, but do it case sensitively if I type capital letters
set ignorecase smartcase wildignorecase

" When I switch buffers just hide the old buffer, don't lose its undo history
set hidden

" When switching buffers, preserve window view.
if v:version >= 700
  autocmd general_vim_au BufLeave * if !&diff | let b:winview = winsaveview() | endif
  autocmd general_vim_au BufEnter * if exists('b:winview') && !&diff |
        \ call winrestview(b:winview) | endif
endif

" Disable the 'ding' error bell sound
set visualbell t_vb=

" Use filler empty lines when one side of diff has content and other doesn't
" Show 3 lines of context around differences (default: 6)
" Open diff's in vertical splits rather than the default horizontal
set diffopt=filler,context:3,vertical

" Ask to save instead of just failing to quit
set confirm

" Not two spaces after . ? etc on line joins, only one
set nojoinspaces

set listchars=tab:▸\ ,nbsp:␣,trail:\ ,extends:…,precedes:…
set matchpairs+=<:>

" Indented lines' wrapped parts are also shown indented
set breakindent

" Show as much of the last line in a window as possible, then show @@@
set display=lastline

" Options for netrw file browser
let g:netrw_banner         = 0
let g:netrw_keepdir        = 0
let g:netrw_liststyle      = 3
let g:netrw_sort_options   = 'i'
let g:netrw_fastbrowse     = 2
let g:netrw_special_syntax = 1

" GUI Options {{{2
if has('gui_running')
  set guioptions-=g
  set guioptions-=m
  set guioptions-=t
  set guioptions-=T
  " Cursor Options {{{3
  " Common options for all cursor modes
  set guicursor=a:Cursor/lCursor-blinkwait1500-blinkon400-blinkoff400
  " Cursor during normal-visual-commandlinenormal modes
  set guicursor=n-v-c:hor10
  " Cursor during insert mode
  set guicursor+=i:ver10
  " Cursor during commandlineinsert mode
  set guicursor+=ci:ver20
  " Cursor during replace-commandlinereplace modes
  set guicursor+=r-cr:block
  " Cursor during waitingforoperation mode
  set guicursor+=o:hor50
  " Cursor during showmatch mode within insert mode
  set guicursor+=sm:block-Cursor-blinkwait175-blinkoff150-blinkon175",
  if has('gui_win32')
    set guifont=Source\ Code\ Pro:h13:cDEFAULT,Consolas:h13:cDEFAULT
    set shellslash
    au FileType vundle setlocal noshellslash
  elseif has('gui_gtk2') || has('x11')
    set guifont=Dejavu\ Sans\ Mono\ 13
  endif
endif

" When splitting horizontally, put the new window below current one
" (mainly for terminal)
set splitbelow
" 15 rows high is plenty for terminal
set termwinsize=15x0

" set zsh for :terminal and ! commands, if available
if executable("zsh")
  set shell=zsh
endif

if filereadable("/usr/share/dict/british")
  set dictionary=/usr/share/dict/british
else
  set dictionary=/usr/share/dict/words
endif

" experimental, let's see if this messes anything up with current setup
set autochdir

" Disable a lot of unnecessary internal plugins (also from lervag's vimrc)
let g:loaded_2html_plugin = 1
let g:loaded_getscriptPlugin = 1
"let g:loaded_gzip = 1
let g:loaded_logipat = 1
let g:loaded_rrhelper = 1
let g:loaded_spellfile_plugin = 1
let g:loaded_tarPlugin = 1
let g:loaded_vimballPlugin = 1
"let g:loaded_zipPlugin = 1

" In case it's unset by OS
set modeline

iabbrev teh the
iabbrev taht that
iabbrev soemthing something
iabbrev thign thing
iabbrev constnatly constantly
iabbrev emotinal emotional
iabbrev mroe more
iabbrev chors chores 
iabbrev chorse chores 
iabbrev toehr other

" Don't auto move cursor to start of line after certain commands (for eg. C-F) 
set nostartofline

" Basic Vim Mappings and Commands {{{1
" Command to quickly open this file
command! -nargs=1 -complete=file E call EditInNewWindow(<f-args>)
function! EditInNewWindow(filename)
  if executable("gvim")
    silent execute "!gvim ".a:filename
  else
    edit a:filename
  endif
endfunction

command! Vrc :execute "E ".resolve($MYVIMRC)

command! -nargs=? SaveSession call SaveSessionFn(<f-args>)
function! SaveSessionFn(...)
  set sessionoptions=buffers,curdir,folds,help,tabpages,winsize,winpos
  if a:0 == 0
    exe "mksession! $HOME/.vim/savedsessions/lastsession.vim"
  else
    exe "mksession! $HOME/.vim/savedsessions/" . a:1 . ".vim"
  endif
endfunction
command! -nargs=? -complete=customlist,SessionFilesComplete LoadSession call LoadSessionFn(<f-args>)
function! LoadSessionFn(...)
  if a:0 == 0
    exe "source $HOME/.vim/savedsessions/lastsession.vim"
  else
    exe "source " . a:1
  endif
endfunction
function! SessionFilesComplete(A,L,P)
  return split(glob("$HOME/.vim/savedsessions/" . a:A . "*"), "\n")
endfun

" Simple ripgrep integration for now, from
" https://github.com/alejandrogallo/vim-ripgrep
function! Rg(...)
  let l:output = system("rg -S --vimgrep ".join(a:000, " "))
  let l:list = split(l:output, "\n")
  let l:ql = []
  for l:item in l:list
    let sit = split(l:item, ":")
    call add(l:ql,
          \ {"filename": sit[0], "lnum": sit[1], "col": sit[2], "text": sit[3]})
  endfor
  call setqflist(l:ql, 'r')
  cwindow
endfunction
command! -nargs=* Rg call Rg(<q-args>)

" F6 switches windows
map <F6> <C-W>p
" Make F6 work in insert mode too
imap <F6> <ESC><F6>

" Ctrl Tab and Ctrl Shift Tab for buffer switching
map <C-Tab> :bn<CR>
imap <C-Tab> <ESC>:bn<CR>
map <C-S-Tab> :bp<CR>
imap <C-S-Tab> <ESC>:bp<CR>

" And Space+d to delete this buffer quickly
nmap <leader>d :bd<CR>
" Also Q, until I think of a better use for it (this is better than Ex mode
" anyway)
nmap Q :bd<CR>

" - saves
nmap - :up<CR>
" Make F2 save insert mode, and take me back to insert mode
imap <F2> <ESC>:up<CR>a
" and for consistency
nmap <F2> :up<CR>

" Fold and unfold with shift-<Space>
nmap <S-Space> zA

" Switch 0 and ^ since we most often want to go to start of text rather than
" line itself, and 0 is much easier to type
nnoremap 0 ^
nnoremap ^ 0

" Clear search highlighting by pressing leader-L (similar to Ctrl-L for
" clearing in console)
nnoremap <leader>l :nohlsearch<CR>

" Turn ; into the ex mode initiator
nnoremap ; :
vnoremap ; :
" Usually ; finds next f match, now let , do that
nnoremap , ;
vnoremap , ;
" And let the unused backspace key do the reverse f match work of ,
nnoremap <bs> ,
vnoremap <bs> ,

" Work with system clipboard simply with leader
vmap <leader>y "+y
vmap <leader>d "+d
vmap <leader>p "+p
vmap <leader>P "+P
nmap <leader>Y "+Y
nmap <leader>D "+dd
nmap <leader>p "+p
nmap <leader>P "+P

" Insert today's date, two different formats
inoremap ;d <C-R>=strftime('%d/%m/%y')<CR>
inoremap ;D <C-R>=strftime('%Y.%m.%d')<CR>

" Insert current time (there's also a snippet that does this, but this is a
" backup in case I don't have snipmate or such installed somewhere
inoremap ;t ##<Space><C-R>=strftime("%H:%M")<CR><CR>
inoremap ;T <C-R>=strftime("%H:%M")<CR>

" Open netrw explorer in vertical split similar to NERDTree
map <F10> :Lexplore<CR>

" Mapping to switch to current file's directory
" (autochdir is erratic and some plugins don't like it)
nnoremap <leader>cd :lcd %:p:h<CR>
vnoremap <leader>cd :lcd %:p:h<CR>

inoremap jk <ESC>

" Display the runtimepath entries in a new buffer, one entry per line
nnoremap <leader>rtp :below new \| execute append(0, &runtimepath) \| s/,/\r/ \| setlocal buftype=nofile<CR>

" Map Alt-number to open that numbered buffer
" https://stackoverflow.com/a/327463/8127
let c = 1
while c <= 9
  execute "nnoremap <A-" . c . "> :" . c . "b\<CR>"
  let c += 1
endwhile

" In terminal, shift-pageup takes to Normal mode, which means further
" shift-pageups scroll up as expected
if exists(":tmap")
  tnoremap <S-PageUp> <C-W>N
endif

" Preserve cursor position after line Join
nnoremap J mzJ`z

nnoremap dp     dp]c
nnoremap do     do]c
" maybe there's a better use for this?
nnoremap '      ` 

" Select the text that was just copied or pasted
nnoremap gV     `[V`]

nmap vv V

" Navigate folds (also from lervag's vimrc)
nnoremap          zv zMzvzz
nnoremap <silent> zj zcjzOzz
nnoremap <silent> zk zckzOzz

" Easy insertion of emojis with C-K
execute 'digraph x( 128544'
execute 'digraph :) 128578'
execute 'digraph :D 128512'
execute 'digraph =D 128513'
execute 'digraph :P 128523'
execute 'digraph :\| 128528'
execute 'digraph =\| 128529'
execute 'digraph :O 128558'
execute 'digraph :/ 128533'
execute 'digraph :( 128577'
execute 'digraph =( 128543'
execute 'digraph :E 128556'
"vanakkam/namaste:
execute 'digraph /\ 128591'
"shrug:
execute 'digraph \/ 129335' 
"yellow heart (since red seems to have display issues):
execute 'digraph <3 128155' 

" Move in insert mode with C-L and C-H, for convenience
inoremap  l
inoremap  h
" Keep backspace available via 
inoremap  

" Filetype Specific Settings {{{1
augroup filetype_au
  " Show pydoc help for current word when shift-K is pressed
  autocmd FileType python setlocal keywordprg=pydoc

  autocmd FileType perl setlocal keywordprg=perldoc\ -f
  let perl_include_pod   = 1    "include pod.vim syntax file with perl.vim
  let perl_extended_vars = 1    "highlight complex expressions such as @{[$x, $y]}
  let perl_sync_dist     = 250  "use more context for highlighting

  autocmd FileType ruby setlocal keywordprg=ri\ -T tabstop=2 shiftwidth=2

  " When there is a .sh file, assume it's bash
  let g:is_bash = 1

  " Treat .tex files as latex by default instead of 'plaintex'
  let g:tex_flavor='latex'
  if has("win32")
    let g:Tex_UsePython=0
  endif

  " Todo.txt plugin config {{{3
  " (could go in After-plugin part, but not sure how to test for todo.txt
  " plugin presence cleanly, and the existence of todo filetype most likely
  " indicates it anyway.)
  " Use todo#Complete as the omni complete function for todo files
  au filetype todo setlocal omnifunc=todo#Complete
  " Auto complete projects
  au filetype todo imap <buffer> + +<C-X><C-O>
  " Auto complete contexts
  au filetype todo imap <buffer> @ @<C-X><C-O>
  " Don't open the (seemingly useless) Scratch window during completion
  au filetype todo setlocal completeopt-=preview
  au filetype todo setlocal completeopt+=noinsert
  au filetype todo setlocal nofoldenable

  " Conceal Markdown syntax and display like output, when possible
  " Maybe worth setting conceallevel just globally, but for now we'll keep it
  " limited
  au filetype markdown setlocal conceallevel=2
  " Make end-of-line two spaces less jarring
  au filetype markdown highlight! default link mkdLineBreak ColorColumn
  " ge is useful to go to end of previous word, don't let markdown.vim take it
  " over (TODO map something to <Plug>Markdown_EditUrlUnderCursor instead, if plugin
  " sees that it doesn't set ge)
  au filetype markdown unmap <buffer> ge
augroup END

" Plugin-Specific Options {{{1
" Plugin Option Variable {{{2
" These ones needs to happen in .vimrc directly, as they might affect the
" forthcoming scripts' functioning. And it's just variable assignments, so
" no error even if no plugin

"Bullets Options
let g:bullets_max_alpha_characters = 0  " disable alphabetic bullets
"let g:bullets_mapping_leader = "\\"
let g:bullets_outline_levels = ['std*', 'std-', 'std+']

" CtrlP Options {{{3
let g:ctrlp_working_path_mode   = 'c'
let g:ctrlp_show_hidden         = 1
let g:ctrlp_mruf_max            = 100
let g:ctrlp_extensions          = ['tag', 'rtscript', 'changes',
      \ 'mixed', 'bookmarkdir']
let g:ctrlp_match_window        = 'order:ttb,min:1,max:10,results:100'
let g:ctrlp_use_caching         = 1
let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_max_files           = 1024
let g:ctrlp_custom_ignore       = {
      \ 'dir':  '\v[\/]\.(git|hg|svn|mypy_cache)$',
      \ 'file': '\v\.(exe|so|dll)$',
      \ }
if has("win32")
  let g:ctrlp_mruf_case_sensitive = 0
endif

" Airline Options {{{3
let g:airline#extensions#tabline#enabled        = 1
let g:airline#extensions#tabline#show_buffers   = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#show_tabs      = 0
let g:airline#extensions#csv#column_display     = 'Name'

" UltiSnips Options {{{3
let g:UltiSnipsExpandTrigger = '<C-Space>'
let g:UltiSnipsListSnippets = '<C-S-Space>'
let g:UltiSnipsJumpForwardTrigger = '<Tab>'
let g:UltiSnipsJumpBackwardTrigger = '<S-Tab>'

" Julia-vim Options {{{3
let g:latex_to_unicode_file_types = ["julia", "markdown"]

" Ledger Options {{{3
let g:ledger_date_format = '%Y.%m.%d'
let g:ledger_default_commodity = 'Rs'
let g:ledger_commodity_sep = ' '
let g:ledger_fillstring = '  ~'
let g:ledger_extra_options = '--pedantic --explicit'
autocmd FileType ledger nnoremap <buffer> == :LedgerAlign<CR>
autocmd FileType ledger vnoremap <buffer> = :LedgerAlign<CR>
" Include : in keyword characters so account completion works as expected
autocmd FileType ledger setlocal iskeyword+=:

" Vim-Markdown Options {{{3

let g:vim_markdown_new_list_item_indent = 0
" Fold, but fold less
let g:vim_markdown_folding_level = 2
" Folding leads to weird editing issues, scroll position changing mid edit and
" currently editing section getting folded mid-edit. So disable it.
" Let's try folding again 28/01/22 nope, same issues still 03/02/22
let g:vim_markdown_folding_disabled = 1
" Strikethrough with ~~
let g:vim_markdown_strikethrough = 1
" Enable latex syntax
let g:vim_markdown_math = 1

" wiki.vim Options {{{3

let g:wiki_filetypes = ['md', 'wiki']
let g:wiki_link_extension = '.md'
let g:wiki_link_target_type = 'md'
" to not interfere with <bs> mapping
let g:wiki_mappings_local = {
      \ '<plug>(wiki-link-return)' : '<M-Left>',
      \}
" not all markdown files should be treated as wiki files
"let g:wiki_global_load = 0 #unfortunately, this disables having multiple wikis
" TODO: disable global load, then WikiEnable when in $REPOS . '/LearnWiki/'
if ! empty($REPOS)
  let g:wiki_root = $REPOS . '/MasterRef/'
endif
if ! empty($ZOTEROROOT)
  let g:wiki_zotero_root = $ZOTEROROOT
endif

" ERegex Options {{{3
" Don't take over normal / and ? (as eregex doesn't support incsearch)
let g:eregex_forward_delim  = '<leader>/'
let g:eregex_backward_delim  = '<leader>?'

" After-Plugin Settings {{{2
" These are "supposed to be" set in after/plugin directory, but then
" cross-platform synchronization would get even messier. So, au VimEnter it is.

function! SetPluginConfigNow()

  " CtrlP Config {{{3
  if exists(":CtrlP")
    " to be able to call CtrlP with default search text
    function! CtrlPWithText(search_text, ctrlp_command_end)
      execute ':CtrlP' . a:ctrlp_command_end
      call feedkeys(a:search_text)
    endfunction

    "let g:ctrlp_map = '<Space>f' "doesn't really work, so...
    nmap <leader>f   :CtrlP<CR>
    nmap <leader>or  :CtrlPRoot<CR>
    nmap <leader>b   :CtrlPBuffer<CR>
    nmap <leader>ot  :CtrlPTag<CR>
    nmap <leader>oV  :CtrlPRTS<CR>
    nmap <leader>oc  :CtrlPChange<CR>
    nmap <leader>om  :CtrlPMRU<CR>
    nmap <leader>oa  :CtrlPMixed<CR>
    nmap <leader>ob  :CtrlPBookmarkDir<CR>

    " CtrlP with default text
    nmap <leader>owt  :call CtrlPWithText(expand('<cword>'), 'Tag')<CR>
    nmap <leader>owf  :call CtrlPWithText(expand('<cword>'), '')<CR>
    nmap <leader>of   :call CtrlPWithText(expand('<cfile>'), '')<CR>
  endif

  " Gundo Config {{{3
  if exists(":GundoToggle")
    nnoremap <leader>gu :GundoToggle<CR>
  endif

  " SpeedDating Config {{{3
  if exists(":SpeedDatingFormat")
    1SpeedDatingFormat %Y.%m.%d
    2SpeedDatingFormat %d/%m/%y
    3SpeedDatingFormat %H:%M
    " Remove roman numeral support, interferes with other uses of C-A C-X
    SpeedDatingFormat! %v
    SpeedDatingFormat! %^v

    " Swapit Fallback to Speeddating {{{4
    nmap <Plug>SwapItFallbackIncrement <Plug>SpeedDatingUp
    nmap <Plug>SwapItFallbackDecrement <Plug>SpeedDatingDown
    vmap <Plug>SwapItFallbackIncrement <Plug>SpeedDatingUp
    vmap <Plug>SwapItFallbackDecrement <Plug>SpeedDatingDown

  endif

  " Tabular Config (thanks vimcasts) {{{3
  if exists(":Tabularize")
    nmap <leader>a= :Tabularize /=<CR>
    vmap <leader>a= :Tabularize /=<CR>
    nmap <leader>a: :Tabularize /:\zs<CR>
    vmap <leader>a: :Tabularize /:\zs<CR>
  endif

  " Allow PCRE in Tabularize if eregex E2v() is available
  if exists(':Tabularize') && exists(':E2v')
    function! AlignByRE() range
      let range  = a:firstline . ',' . a:lastline
      let cmd    = range
      let re = input('Enter PCRE to align by: ')
      if re == ""
        " Run empty to use previous pattern
        let cmd .= 'Tabularize'
        execute cmd
      else
        let vim_re  = E2v(re)
        let cmd    .= 'Tabularize/' . vim_re
      endif
      execute cmd
    endfunction
    nnoremap <leader>A :call AlignByRE()<CR>
    vnoremap <leader>A :call AlignByRE()<CR>
  endif

endfunction

augroup vimenter_au
  au VimEnter * call SetPluginConfigNow()
augroup END

" Final Vim-Startup Commands {{{1

" Modelines {{{1
" vim: fdm=marker
