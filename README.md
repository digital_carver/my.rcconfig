
First run:

    ./stow.sh -nvv bash git julia kde nvim perl ruby screen tidy todosh vim zsh

check the output, then without the `-n`:

    ./stow.sh -vv bash git julia kde nvim perl ruby screen tidy todosh vim zsh

and: 

    cp __.profile_local_TEMPLATE ~/.profile_local


The `__skel` files can go under /etc/skel if desired. 
