#
# Executes commands at the start of an interactive session.

# Source Prezto if available
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# not sure what difference this makes though, since the shell mode in 
# Julia is pretty crippled anyway
export JULIA_SHELL=`command -v zsh`

alias yt='yt-dlp'
alias gdf='git diff'
alias hi='highlight --out-format=truecolor'
alias rg='rg --max-columns 201'
alias jlquick='JLQUICK=1 JULIA_PKG_PRECOMPILE_AUTO=0 julia --compile=min --optimize=0 --threads=auto --inline=no'
alias jlq='jlquick'
alias jlp='julia --project=. --threads=auto'
alias jlt='julia --threads=auto -e "import Pkg; Pkg.activate(; temp = true)" -i'
alias jli="julia --project=@Interpret -e 'using JuliaInterpreter; import Pkg; Pkg.activate(\".\")' -i"
alias jl='julia --threads=auto'
alias xo='xdg-open'
alias pipeedit='vipe' #just because this name is much easier to remember 

if command -v bpython &> /dev/null; then
  alias py='bpython'
  alias py3='python3'
elif command -v python3 &> /dev/null; then
  alias py='python3'
else
  alias py='python'
fi

# TODO handle - maybe prefer - neovim if available
if command -v gvim &> /dev/null ; then #should also check if GUI available
  export VISUAL=gvim
  export EDITOR=gvim
  alias v='gview -'
elif  command -v vim &> /dev/null ; then
  export VISUAL=vim
  export EDITOR=vim
  alias v='view -c "set noautochdir" -'
fi

alias ok='okular'

alias whichproject='perl $REPOS/whichproject/whichproject.pl'

alias tut='EDITOR=nvim tut'

setopt auto_pushd
unsetopt correct

bindkey ' ' magic-space

# Make the repos directory accessible as ~r
hash -d r=$REPOS

mkcd () {
    command mkdir -p "$@" && cd "$_"
}

# Source local zshrc if available
if [[ -s "${ZDOTDIR:-$HOME}/.zshrc_local" ]]; then
  source "${ZDOTDIR:-$HOME}/.zshrc_local"
fi

