
if [ -f "${HOME}/.bashrc" ]; then
  source "${HOME}/.bashrc"
fi

if [ -f "${HOME}/.profile_local" ]; then
  source "${HOME}/.profile_local"
fi

