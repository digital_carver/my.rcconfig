
" Compatibility options {{{1
" Don't act like old guy vi, give me the features of vim
if &compatible
  set nocompatible
endif
" When moving the cursor up or down just after inserting indent for 'autoindent',
" do not delete the indent.
set cpoptions+=I
" Allow editing command pasted from clipboard using :@r (where r is register)
set cpoptions-=e
" When using "w!" while the 'readonly' option is set, don't reset 'readonly'.
" set cpoptions+=Z

" Fundamental Pre-Plugin options {{{1

" Space is the <leader> key
let mapleader=" "
" And Tab the <localleader> key
let maplocalleader="	"

" Plugins to install {{{1
" tpope/vim-unimpaired
" ledger

" General (non-plugin-specific, non-filetype-specific) Vim settings {{{1

" Syntax highlighting
syntax enable

" Ignore case generally, but do it case sensitively if I type capital letters
set ignorecase smartcase wildignorecase

" When I switch buffers just hide the old buffer, don't lose its undo history
set hidden

" Ask to save instead of just failing to quit
set confirm

set shiftwidth=2
set tabstop=2
set expandtab

" Not two spaces after . ? etc on line joins, only one
set nojoinspaces

" When splitting horizontally, put the new window below current one
" (mainly for terminal) TODO checkif remains valid for neovim
set splitbelow

" set zsh for :terminal and ! commands, if available
if executable("zsh")
  set shell=zsh
endif

if filereadable("/usr/share/dict/british")
  set dictionary=/usr/share/dict/british
else
  set dictionary=/usr/share/dict/words
endif

" I find this the most intuitive
set autochdir

" In case it's unset by OS
set modeline

set diffopt+=indent-heuristic,algorithm:minimal

set number
set relativenumber

augroup general_vim_au
  autocmd TermOpen * startinsert
  autocmd BufEnter * set formatoptions-=o
augroup END

let &showbreak = '…'

set whichwrap=b,s,<,>,[,]

set showcmd

" Don't auto-commentify the next line after a comment
set formatoptions-=ro

set colorcolumn=92
" Basic Mappings and Commands {{{1

" Ctrl Tab and Ctrl Shift Tab for buffer switching
map <C-Tab> :bn<CR>
imap <C-Tab> <ESC>:bn<CR>
map <C-S-Tab> :bp<CR>
imap <C-S-Tab> <ESC>:bp<CR>

" And Space+d to delete this buffer quickly
nmap <leader>d :bd<CR>
" Also Q, until I think of a better use for it (this is better than Ex mode
" anyway)
nmap Q :bd<CR>

" - saves
nmap - :up<CR>
" Make F2 save insert mode, and take me back to insert mode
imap <F2> <ESC>:up<CR>a
" and for consistency
nmap <F2> :up<CR>

" Fold and unfold with shift-<Space>
nmap <S-Space> zA

" Switch 0 and ^ since we most often want to go to start of text rather than
" line itself, and 0 is much easier to type
nnoremap 0 ^
nnoremap ^ 0

" Turn ; into the ex mode initiator
nnoremap ; :
vnoremap ; :
" Usually ; finds next f match, now let , do that
nnoremap , ;
vnoremap , ;
" And let the unused backspace key do the reverse f match work of ,
nnoremap <bs> ,
vnoremap <bs> ,

" Work with system clipboard simply with leader
vmap <leader>y "+y
vmap <leader>d "+d
vmap <leader>p "+p
vmap <leader>P "+P
nmap <leader>Y "+Y
nmap <leader>D "+dd
nmap <leader>p "+p
nmap <leader>P "+P

" make Y consistent with C and D
nmap Y y$

" Insert today's date, two different formats
inoremap ;d <C-R>=strftime('%d/%m/%y')<CR>
inoremap ;D <C-R>=strftime('%Y.%m.%d')<CR>

" Insert current time (there's also a snippet that does this, but this is a
" backup in case I don't have snipmate or such installed somewhere
inoremap ;t ##<Space><C-R>=strftime("%H:%M")<CR><CR>

" Open netrw explorer in vertical split similar to NERDTree
map <F10> :Lexplore<CR>

" Display the runtimepath entries in a new buffer, one entry per line
nnoremap <leader>rtp :below new \| execute append(0, &runtimepath) \| s/,/\r/ \| setlocal buftype=nofile<CR>

" Map Alt-number to open that numbered buffer (upto buffer num 19, seems
" a reasonable limit)
let c = 1
while c <= 19
  execute "nnoremap <A-" . c . "> :" . c . "b\<CR>"
  let c += 1
endwhile

" In terminal, shift-pageup takes to Normal mode, which means further
" shift-pageups scroll up as expected
" (Unfortunately doesn't seem to work in nvim)
tnoremap <S-PageUp> <C-\><C-N>
tnoremap <C-W><C-W> <C-\><C-N><C-W><C-W>

" Preserve cursor position after line Join
nnoremap J mzJ`z

nnoremap dp     dp]c
nnoremap do     do]c
" maybe there's a better use for this?
nnoremap '      ` 

" Select the text that was just copied or pasted
nnoremap gV     `[V`]

nmap vv V

" Navigate folds (also from lervag's vimrc)
nnoremap          zv zMzvzz
nnoremap <silent> zj zcjzOzz
nnoremap <silent> zk zckzOzz

nnoremap <silent> <leader>= migg=G`i

" kakoune-inspired. gi is too useful though, and gh for 0 will be basically
" unused anyway
nnoremap gl $
nnoremap gh ^

" Clear search highlighting by pressing leader+l (similar to Ctrl-L for
" clearing in console)
nnoremap <leader>l :nohlsearch<CR>
" leader+shift-L to force reload the file (invoke global reparsing, etc.)
nnoremap <leader>L :e<CR>

" I usual want terminal in a split window
command! Term split +terminal

" Modelines {{{1
" vim: fdm=marker
