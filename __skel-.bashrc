#Skeleton bashrc to be used for any new user


### If not running interactively, don't do anything
case $- in
    *i*)
        ;;
    *) return
        ;;
esac

### Functions for internal use here

# Check whether a command is availabel for use in this system
cmd_exists () {
    type "$1" >/dev/null 2>&1
}

# Add the given argument to PATH if it isn't already there
add_to_PATH () {
    if [[ $# < 1 ]]; then continue; fi
    if [ -z "$1" ]; then continue; fi #skip nonexistent directory
    #(the := sets PATH to $new if its empty currently)
    case ":${PATH:=$1}:" in
        *:$1:*)
            ;;
        *)
            if [[ $# -eq 2 && "$2" == "prefix" ]]; then
                export PATH="$1:$PATH"
            else
                export PATH="$PATH:$1"
            fi
            ;;
    esac
}

### Shell Options

HISTSIZE=10000
HISTFILESIZE=20000

# Don't wait for job termination notification
set -o notify

# Make bash append rather than overwrite the history on disk
shopt -s histappend

## Completion options

# Define to avoid stripping description in --option=description of './configure --help'
COMP_CONFIGURE_HINTS=1
#
# Define to avoid flattening internal contents of tar files
# (Messes with completion to the tar files themselves) http://tinyurl.com/pcgoqaw
# COMP_TAR_INTERNAL_PATHS=1
#
# Uncomment to turn on programmable completion enhancements.
# Any completions you add in ~/.bash_completion are sourced last.
[[ -f /etc/bash_completion ]] && . /etc/bash_completion

## History Options
#
# Don't put duplicate lines in the history.
export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
#
# Ignore some controlling instructions
# HISTIGNORE is a colon-delimited list of patterns which should be excluded.
# The '&' is a special pattern which suppresses duplicate entries.
# export HISTIGNORE=$'[ \t]*:&:[fb]g:exit'
# Ignore empty space-only cmds, duplicates, fg, bg, exit, ls and cd
export HISTIGNORE=$'[ \t]*:&:[fb]g:exit:ls:cd'
#
# Whenever displaying the prompt, write the previous line to disk
# export PROMPT_COMMAND="history -a"

### Aliases
#
# Some people use a different file for aliases
# if [ -f "${HOME}/.bash_aliases" ]; then
#   source "${HOME}/.bash_aliases"
# fi
#

alias s='sudo'
alias so='source' #habit from vim

# Interactive operation...
alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'
#
# Misc :)
alias less='less -r'                          # raw control characters
alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
alias gd='git diff'
alias tiidy="html-beautify -r -s 2 -n -w 100"
#
# Some shortcuts for different directory listings
alias ls='ls -hF --color=tty'                 # classify files in colour
alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'
alias ll='ls -l'                              # long list
alias la='ls -A'                              # all but . and ..
alias l='ls -CF'                              # show type indicators, in columns

alias gopen='gnome-open'

### Functions for external use
#
# This function defines a 'cd' replacement function capable of keeping,
# displaying and accessing history of visited directories, up to 10 entries.
# Uses either pushd or popd internally, depending on arguments.
# Thanks to: Petar Marinov, http:/geocities.com/h2428, this is public domain
pd_func ()
{
    local x2 the_new_dir adir index
    local -i cnt

    if [[ $1 ==  "--" || -z $1 ]]; then
        dirs -v
        return 0
    fi

    the_new_dir=$1

    if [[ ${the_new_dir:0:1} == '-' ]]; then
        # Extract dir N from dirs
        index=${the_new_dir:1}
        [[ -z $index ]] && index=1
        adir=$(dirs +$index)
        [[ -z $adir ]] && return 1
        the_new_dir=$adir
    fi

    # '~' has to be substituted by ${HOME}
    [[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"

    # Now change to the new dir and add to the top of the stack
    pushd "${the_new_dir}" > /dev/null
    [[ $? -ne 0 ]] && return 1
    the_new_dir=$(pwd)

    # Trim down everything beyond 11th entry
    popd -n +11 2>/dev/null 1>/dev/null

    # Remove any other occurence of this dir, skipping the top of the stack
    for ((cnt=1; cnt <= 10; cnt++)); do
        x2=$(dirs +${cnt} 2>/dev/null)
        [[ $? -ne 0 ]] && return 0
        [[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
        if [[ "${x2}" == "${the_new_dir}" ]]; then
            popd -n +$cnt 2>/dev/null 1>/dev/null
            cnt=cnt-1
        fi
    done

    return 0
}

alias pd=pd_func

# Give a GUI alert in notifications panel when a command completes
# Usage: `your_longrunning_command & guialert`
# (or `; guialert` or `| guialert` if you want the process to remain foreground)
guialert_func () {
    if cmd_exists notify-send ; then
        if [[ $? == 0 ]] ; then
            icon="terminal"
        else
            icon="error"
        fi
        $last_cmd = $(history|tail -n1|sed -e 's/^\s*[0-9]\+\s*//;s/[;&|]\s*guialert$//')
        notify-send --urgency=low -i $icon "$last_cmd has completed"
    fi
}
alias guialert=guialert_func

clean_PATH () {
    cmd_exists perl || >&2 echo "Perl doesn't seem to be available here
                        (and I can't be arsed to write a shell-only version)"
    export PATH=$(perl -E '
    @paths = split(/:/, $ENV{PATH});
    exit unless(@paths);
    @uniq_paths{@paths} = 1; #hash with paths as keys
    $new_path = shift @paths;
    delete $uniq_paths{$new_path};
    #cannot just do join(keys(..)) since we want to try and preserve order
    for $path (@paths) {
        if (exists($uniq_paths{$path})) {
            $new_path .= ":$path";
            delete $uniq_paths{$path};
        }
    }
    print $new_path;
    ')
}

pl () {
    # Call with --bg to run program in background
    if [[ "$1" == "--bg" ]] ; then
        shift                           #remove the --bg arg from $@
        nohup perl "$@" 2>nohup.err &
    else
        perl "$@"
        guialert
    fi
}

pld () {
    perl -d "$@"
}

plc () {
    perl -wc "$@"
}

### OS specific stuff

if [[ "$OSTYPE" == "cygwin" ]]; then
    # Use case-insensitive filename globbing in Windows
    shopt -s nocaseglob
    #Unset Windows' pythonhome to avoid it messing with cygwin's
    unset -v PYTHONHOME
elif [[ "$OSTYPE" == "msys" ]]; then
    # Use case-insensitive filename globbing in Windows
    shopt -s nocaseglob
else
    #always install Perl modules to system paths
    #(only on Unixes since no need in msys and no sudo available in cygwin)
    alias cpanm='cpanm --sudo'
fi

### Set some environment variables

#Edited default bashrc to also display current time (the \t after \n)
PS1='[\t] \[\e[32m\]\u@\h \[\e[33m\]\w\[\e[0m\]\n\$ '

#Anywhere, anytime! :)
EDITOR=vim

### source the unsynced local bashrc last so it can override anything it wants to
if [ -f "${HOME}/.bashrc_local" ] ; then
    source "${HOME}/.bashrc_local"
fi

### Modelines
## Folding - three # for top-level fold, two for sub-fold
# vim:fdm=expr:fdl=0
# vim:fde=getline(v\:lnum)=~'^##'?'>'.(4-matchend(getline(v\:lnum),'##*'))\:'='
