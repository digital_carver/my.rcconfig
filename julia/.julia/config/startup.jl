

module Sundar

using Markdown
more(content) = more(repr("text/plain", content))
more(content::Markdown.MD) = more(Markdown.term(stdout, content))
function more(content::AbstractString)
  # FIXME $(content) is dangerous, not just interpolation but execution. What do? 
  # perhaps write the contents to a mktemp file and then use InteractiveUtils.less
  inJupyter ? content : run(pipeline(`echo $(content)`, `less`))
  nothing
end

function grep(needle::Function, haystack) 
  alllines = split(string(haystack), '\n')
  matchlines = filter(needle, alllines) 
  foreach(println, matchlines)
end
grep(needle, haystack) = grep(contains(needle), haystack)
grep(needle) = haystack -> grep(needle, haystack)

using Dates: format, Minute, Millisecond, Second, canonicalize, now
function timer(; starttext = "", timertext = "")
  printnow() = print(format(now(), "HH:MM:SS"))
  print(starttext); printnow(); println()
  c = nothing
  @async c = readline()
  while isnothing(c)
    print('\r')   # go back to start of line, overwrite existing
    print(timertext); printnow()
    sleep(1)
  end
end

countdown(duration::Int; kwargs...) = countdown(Second(duration); kwargs...)
function countdown(duration = Minute(5); updatefreq = 10)
  try
    durationms = Millisecond(duration)
    starttime = now()
    print("$duration starting... NOW!")
    clearline = '\r' * ' '^100
    elapsedtime = now() - starttime
    while elapsedtime < durationms
      sleep(min(Millisecond(1000updatefreq), durationms - elapsedtime))
      elapsedtime = now() - starttime
      print(clearline, "\r$(round(durationms - elapsedtime, Second) |> canonicalize) remaining")
    end
    print(clearline)
    timer(starttext = """\rTIME'S UP!
          Countdown started $duration ago, now it's """, timertext = "Extra time until: ")
  catch e
    e isa InterruptException ? println() : rethrow()
  end
end
end
 
function initrepl(replobj)
  if !haskey(ENV, "JULIA_EDITOR")
    ENV["JULIA_EDITOR"] = "gvim"
  end

  push!(LOAD_PATH, "@InteractiveEnv")

  if get(ENV, "JLQUICK", 0) == 0  && false #disabled for v1.8 until bugs are fixed
    try 
      @eval using OhMyREPL
      @async begin
        # reinstall keybindings to work around https://github.com/KristofferC/OhMyREPL.jl/issues/166
        sleep(3)
        OhMyREPL.Prompt.insert_keybindings()

        # and do this here too, to avoid world age problems
        OhMyREPL.enable_autocomplete_brackets(false)
        #OhMyREPl.enable_pass!("SyntaxHighlighter", false) #for performance

        @eval import Dates
        OhMyREPL.input_prompt!(:green) do 
          Dates.format(Dates.now(), "H:MM:SS ⌚") * " julia> "
        end
      end
    catch e
      @warn "error using OhMyREPL" e
    end
  end

end

atreplinit(initrepl)


